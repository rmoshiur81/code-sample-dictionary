<?php
/**
 * Created by Md Moshiur Rahman.
 * User: Moshiur
 * @author Md. Moshiur Rahman
 * contact 01731395559
 */

namespace App\Main;

class Main {

	protected $dbConnect;

	public function __construct() {

		if (file_exists('config.php')){
			$c = include 'config.php';
		}else{
			$c = include '../../config.php';
		}

		$this->dbConnect= new \mysqli($c['datasource']['host'],$c['datasource']['username'],$c['datasource']['password'],$c['datasource']['database']) or die('Mysql database connection error');
		if (!$this->dbConnect) {
			echo "Error: Unable to connect to MySQL." . PHP_EOL;
			echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
			echo "Debugging error: " . mysqli_connect_error() . PHP_EOL;
			exit;
		}
	}


//	title of each page
	public function printTitle($value='Dictionary')
	{
		return $value;
	}
	protected function dbClose(){
		$this->dbConnect->close();
	}


}