<?php
/**
 * Created by Moshiur.
 * User: Moshiur
 * Date: 06-Dec-16
 * Time: 3:23 PM
 * Contact: 01731395559
 */

namespace App\HelperTrait\Session;

/**
 * actual purpose of this trait is to provide most common session methods which are needed frequently
 * this session trait is used in Session class for helping session class by providing most common session related functionality
 */
Trait Session
{
    public static function startSessionMoshiur()
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
    }

    public static function makeSessionMoshiur($key, $value)
    {
        Session::startSessionMoshiur();
        $_SESSION[$key] = $value;
        return true;
    }

    public static function makeAllSessionMoshiur(array $arr)
    {
//        Session::startSessionMoshiur();
        foreach($arr as $key => $value){
            $_SESSION[$key] = $value;
        }
        return true;
    }
    public static function retriveSessionMoshiur($key)
    {
//        Session::startSessionMoshiur();
        return $_SESSION[$key];
    }
    public static function seeAllSessionMoshiur()
    {
//        Session::startSessionMoshiur();
        print_r($_SESSION);
    }
/**
 *   TO DO
 *

**/
    public static function checkSessionMoshiur($key)
    {
//        Session::startSessionMoshiur();
        if(isset($_SESSION[$key])){
            return true;
        }else{
            return false;
        }
    }

    public static function unsetSessionMoshiur($key)
    {
//        Session::startSessionMoshiur();
        unset($_SESSION[$key]);
        return true;
    }

    public static function destroySessionMoshiur()
    {
//        Session::startSessionMoshiur();
        $ConfirDestroy = session_destroy();
        if(!$ConfirDestroy){
            echo "Error occurred while destroying session";
        }else{
            return false;
        }
    }


}