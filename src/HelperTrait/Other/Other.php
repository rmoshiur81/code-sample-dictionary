<?php
/**
 * Created by Md Moshiur Rahman.
 * User: RIMI
 * Date: 5/25/2017
 */

namespace App\HelperTrait\Other;

/**
 * actual purpose of this trait is to provide most common methods which are needed frequently
 */
Trait Other {

	function hasStringMoshiur( $inside, $findMe ) {
		if ( strlen( $findMe ) > 0 ) {
			if ( stristr( $inside, $findMe ) ) {
				$v = str_ireplace( $findMe, "<span style='background-color:yellow;'>$findMe</span>", $inside );

				return $v;
			}
		}

		return $inside;
	}

	function checkDataTypeMoshiur( $param, $type ) {
		if ( $type == gettype( $param ) ) {
			return true;
		}

		return false;
	}


}