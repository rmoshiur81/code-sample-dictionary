<?php
/**
 * Created by Md Moshiur Rahman.
 * User: RIMI
 * Date: 5/26/2017
 */

namespace App\Sql;

use App\Main\Main;
use App\HelperTrait\Session\Session;
use App\HelperTrait\Other\Other;


/**
 * actual purpose of this class is to provide most common sql methods though for dictiionary related sql
 * but other related sql like teachers,students, test etc will be in separate class from this one
 * this class name should be DictionarySql and will be amend in next version related functionality
 */
class Sql extends Main {
	use Session;
	use Other;
	protected $query = '';

	public function __construct() {
		parent:: __construct();
		$this->query;
	}

	protected function queryAsAssocArray( $query ) {
		$queryResult = $this->dbConnect->query( $query );

		$wordsLists = [];
		while ( $allDictionaryData = $queryResult->fetch_assoc() ) {
			$wordsLists[] = $allDictionaryData;
		}

		return $wordsLists;
	}

	//	load this method as dictionary data on load
	protected function dictionaryDataOnLoad( $limit = 300 ) {
		if ( $this->query == '' ) {
			$this->query = "SELECT * FROM bangla_english ORDER BY english ASC  LIMIT $limit";
		}

		return self::queryAsAssocArray( $this->query );

	}

	//	load this method as dictionary data	on search
	protected function dictionarySearch( $searchKey, $limit = 300 ) {
		if ( $this->query == '' ) {
			$this->query = "select * from bangla_english where english like '$searchKey%' or bangla like '$searchKey%' or english_synonym like '%$searchKey%' or bangla_synonym like '%$searchKey%'";
		}

		return self::queryAsAssocArray( $this->query );
	}



	/**
	 * the purpose of this method is loading dictionary when loading page and on search
	 *
	 */
	public function getWordList() {
		$wordList = [];
		if ( isset( $_GET['searchKey'] ) ) {
			$keyword = addslashes( $_GET['searchKey'] );
			if ( ! preg_match( "/^[0-9]*$/", $keyword ) || strlen( $keyword ) > 0 ) {
				$wordList['words'] = self::dictionarySearch( $keyword );
			} else {
				$wordList['words'] = self::dictionaryDataOnLoad();
			}
		} else {
			$wordList = self::dictionaryDataOnLoad();
		}
		$wordList['keyword'] = $keyword;

		return $wordList;
	}


}