<?php
/**
 * Created by Md. Moshiur Rahman.
 * User: RIMI
 * Date: 5/26/2017
 */

namespace App\Session;
use App\HelperTrait\Session\Session as SessionTrait;

class Session{
	use SessionTrait;
}