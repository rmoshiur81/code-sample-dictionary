<?php
/**
 * Created by Md. Moshiur Rahman.
 * User: RIMI
 * Date: 5/26/2017
 */

namespace App\Json;

use App\Sql\Sql;

/**
 * various types of ajaxResponse
 */
class Json extends Sql {
	protected $methodName;

	public function __construct() {
		parent::__construct();
		$this->methodName;
	}

	public function responseToAjaxCall( $param ) {
		Self::$param();
	}

	public function dictionaryDataAsJson() {
		print_r( $this->dictionaryDataOnLoad() );
	}
}
