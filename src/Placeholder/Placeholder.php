<?php

namespace App\Placeholder;

use App\HelperTrait\Other\Other;

/**
 * Created by Md. Moshiur Rahman.
 * User: RIMI
 * Date: 5/25/2017
 */
// placeholder class for controlling and loading pages or contents
class Placeholder {

	private $item;
	const CONTENT_ROOT = 'views/Contents/';

	function __construct() {
		$this->item = isset( $_GET['item'] ) ? $_GET['item'] : '';
	}

	function contentController() {
		if ( isset( $_GET['item'] ) ) {
			$item = $this->item;
			if ( ( ! is_numeric( $this->item ) ) && strlen( $this->item ) > 0 ) {
				switch ( $item ) {
					case 'dictionary':
						include Self::CONTENT_ROOT . "Dictionary.php";
						break;
					case 'give-test':
//						include Self::CONTENT_ROOT . "GiveTest";
						echo "Coming soon";
						break;
					default:
						include Self::CONTENT_ROOT . "Dictionary.php";
				}

			} else {
				include 'views/Contents/404.php';
			}
		}else{
			include Self::CONTENT_ROOT . "Dictionary.php";
		}
	}
}