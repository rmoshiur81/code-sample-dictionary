<?php
include_once( '../../vendor/autoload.php' );

use App\Json\Json;

$dictionaryData = new Json();
if (isset($_GET['method'])){
	$method = filter_var($_GET['method'], FILTER_SANITIZE_STRING);
	$dictionaryData->responseToAjaxCall( $method );
}

