<link rel="stylesheet" media="screen and (min-width:786px)" href="resources/css/largeScreen.css" type="text/css">
<link rel="stylesheet" media="screen and (max-width:785px)"
      href="resources/css/smallScreen.css">
<link rel="stylesheet" href="resources/css/dictionaryCommon.css">
<div class="fix">
    <table width="100%" border="1" cellspacing="0px">
        <thead style="position: static">
        <tr>
            <th class="a">SL NO</th>
            <th class="b">English</th>
            <th class="c">Bangla</th>
            <th class="d">English Synonym</th>
            <th class="e">Bangla Synonym</th>
            <th class="g">Explanation</th>
        </tr>
        <tr>
            <th class='find' colspan="6">
                <form>
                    <span class="k">Find word</span>
                    <input type="text" class="search_input" autofocus/>
                </form>
            </th>
        </tr>
        </thead>
        <tbody class="output">
        </tbody>
    </table>
</div>
<script src="resources/js/dictionary.js"></script>
