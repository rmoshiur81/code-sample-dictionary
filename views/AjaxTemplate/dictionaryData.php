<?php
include_once( '../../vendor/autoload.php' );
use App\Sql\Sql;
use App\Session\Session;

$s = new Session();

$sql          = new Sql();
$wordListsArr = $sql->getWordList();
$wordLists    = $wordListsArr['words'];
$keyword      = $wordListsArr['keyword'];
$sl           = 1;
foreach ( $wordLists as $word_list ) {
	?>
    <tr class='sty'>
        <td class='a'><?= $sl ?></td>
        <td class='b'><?= $sql->hasStringMoshiur( $word_list['english'], $keyword ); ?></td>
        <td class='c'><?= $sql->hasStringMoshiur( $word_list['bangla'], $keyword ); ?></td>
        <td class='d'><?= $sql->hasStringMoshiur( $word_list['english_synonym'], $keyword ); ?></td>
        <td class='e'><?= $sql->hasStringMoshiur( $word_list['bangla_synonym'], $keyword ); ?></td>
        <td class='g'><?= $sql->hasStringMoshiur( $word_list['explanation'], $keyword ); ?></td>
    </tr>
	<?php
	$sl ++;
}
?>
