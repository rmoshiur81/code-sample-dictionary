$(document).ready(function () {

    // ajax request function
    function ajaxWordList(searchKey) {
        $.ajax({
            type: 'GET',
            url: 'views/AjaxTemplate/dictionaryData.php',
            data:{'searchKey':searchKey},
            success: function (data) {
                $('.output').html(data);
            }
        });
    }

    // for bringing Words based on search(){
        $('.search_input').keyup(function () {
           searchKey = $(this).val();
            ajaxWordList(searchKey);
        });
    // }
    // bringing Words on loads();
    ajaxWordList('');




    // console.log(dictDataList);
    // var dictTemplate = _.template($("#template").html());
    // var resultingHtml = dictTemplate({dictDataList : dictDataList});
    // $(".words-list").append(resultingHtml);
});