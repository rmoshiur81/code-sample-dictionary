<?php
/**
 * Created by Md Moshiur Rahman.
 * User: Moshiur
 * @author Md. Moshiur Rahman
 */
include_once( 'vendor/autoload.php' );
use App\Placeholder\Placeholder;

$placeholder = new Placeholder();
?>
<!DOCTYPE html>
<html>
<head>
    <title><?php //$main->printTitle( $title ); ?></title>
    <meta charset="utf8">
	<?php include_once( 'views/Template/head.php' ); ?>
</head>

<body>
<header>
    <nav>
		<?php include_once( 'views/Elements/Topbar.php' ); ?>
    </nav>
</header>
<section>
	<?php include_once( 'views/Template/body.php' ); ?>
</section>

</body>
<footer>
	<?php include_once( 'views/Template/footer.php' ); ?>
</footer>
</html>

