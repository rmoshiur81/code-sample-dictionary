<?php
$abc = 'abc';
return [
	'datasource' =>[
		'host' =>'localhost',
		'username' =>'root',
		'password' =>'',
		'database' =>'education_sample',
	],
	'author' => [
		'author' => 'Md. Moshiur Rahman',
		'phone' => '01731395559',
		'website' => 'demoeducation.moshiur.net',
		'address' => 'N/A'
	],
];