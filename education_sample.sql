-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 25, 2017
-- Server version: 10.0.31-MariaDB
--

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `moshiurn_edu`
--

-- --------------------------------------------------------

--
-- Table structure for table `bangla_english`
--

CREATE TABLE `bangla_english` (
  `id` int(10) NOT NULL,
  `english` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `bangla` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `english_synonym` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `bangla_synonym` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `explanation` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bangla_english`
--

INSERT INTO `bangla_english` (`id`, `english`, `bangla`, `english_synonym`, `bangla_synonym`, `explanation`) VALUES
(1, 'Aback', 'à¦¬à¦¿à¦¸à§à¦®à¦¿à¦¤', 'surprise,startle, amaze ,stun ,astonish, take sb aback, astound', 'à¦…à¦¬à¦¾à¦•', ''),
(2, 'Accelerate', 'à¦ªà§à¦°à¦­à¦¾à¦¬à¦¿à¦¤ à¦•à¦°à¦¾', 'Expedite,influence', 'à¦—à¦¤à¦¿ à¦¬à§ƒà¦¦à¦§à¦¿ à¦•à¦°à¦¾', ''),
(3, 'Acclaim', 'à¦¸à§à¦¬à¦¾à¦—à¦¤ à¦œà¦¾à¦¨à¦¾à¦¨à§‹', 'applaud,welcome,praise eagerly', '', ''),
(4, 'Accord', 'à¦°à¦¾à¦œà¦¿ à¦¹à¦“à§Ÿà¦¾', 'Agreement', '', ''),
(5, 'Achievement', 'à¦…à¦°à§à¦œà¦¨', 'Deed, Feat, Work, Exploit', '', ''),
(6, 'Acknowledge', 'à¦¸à§à¦¬à§€à¦•à¦¾à¦° à¦•à¦°à¦¾', 'Admit, Cite, Know, Mention, Notice, Receipt, Recognize', '', ''),
(7, 'Acquisition', 'à¦…à¦°à§à¦œà¦¨', 'Acquirement, Attainment,', '', ''),
(8, 'Acrobat', 'à¦•à¦¸à¦°à¦¤à¦¬à§€à¦¦', 'Gymnast', '', ''),
(9, 'Add', 'à¦¯à§‹à¦— à¦•à¦°à¦¾, à¦¸à¦‚à¦¯à§à¦•à§à¦¤ à¦•à¦°à¦¾', 'Attach, Combine, Join, Integrate, Unite', '', ''),
(10, 'Addiction', 'à¦†à¦¸à¦•à§à¦¤à¦¿,à¦®à¦¾à¦¦à¦•à¦¾à¦¸à¦•à§à¦¤à¦¿', 'Compulsion, Dependence, Habit, Fixation, Obsession', '', ''),
(11, 'Adequate', 'à¦ªà¦°à§à¦¯à¦¾à¦ªà§à¦¤', 'Sufficient,enough', 'à¦¯à¦¥à§‡à¦·à§à¦Ÿ', ''),
(12, 'Admiration', 'à¦ªà§à¦°à¦¶à¦‚à¦¸à¦¾ à¦•à¦°à¦¾', 'High Regard, Praise,Applaud', '', ''),
(13, 'Wage', 'à¦®à¦œà§à¦°à¦¿', ' pay,salary,earnings', '', ''),
(14, 'scoundrel', 'à¦ªà¦¾à¦œà§€', 'bad or dishonest man', 'à¦¬à¦¦à¦®à¦¾à¦¶ à¦²à§‹à¦•, à¦¦à§à¦°à§à¦¬à§ƒà¦¤à§à¦¤', 'someone who cheats or deceives other people'),
(15, 'Waist', 'à¦•à§‹à¦®à¦°', '', '', 'area around the middle of the body between the ribs and the hips'),
(16, 'rib', 'à¦ªà¦¾à¦œà¦°à§‡à¦° à¦¹à¦¾à¦°', '', '', 'plural:ribs'),
(17, 'Vertical', 'à¦–à¦¾à§œà¦¾', 'Upright, perpendicular', '', ''),
(18, 'Verse', 'à¦•à¦¬à¦¿à¦¤à¦¾', 'Rhyme, stanza, canto', '', ''),
(19, 'utility', 'à¦‰à¦ªà¦¯à§‹à¦—à¦¿à¦¤à¦¾', 'Advantage, avail, benefit', '', ''),
(20, 'Tradition', 'à¦à¦¤à¦¿à¦¹à§à¦¯', 'Convention, custom, establish,timehonour', '', ''),
(21, 'Symbol', 'à¦ªà§à¦°à¦¤à§€à¦•', 'indication,symptom,symbol,indicator,signal', 'à¦¨à¦®à§à¦¨à¦¾,à¦šà¦¿à¦¹à§à¦¨', ''),
(22, 'Rural', 'à¦—à§à¦°à¦¾à¦®à§€à¦£', 'Comntryside,rustic', '', ''),
(23, 'Quiet', 'à¦¶à¦¾à¦¨à§à¦¤', 'soft,faint,inaudible,hushed,muffled', 'à¦¨à§€à¦°à¦¬', ''),
(24, 'Provide', 'à¦¸à¦°à¦¬à¦°à¦¾à¦¹ à¦•à¦°à¦¾', 'give,supply,issue,lend', 'à¦¯à§‹à¦—à¦¾à¦¨ à¦¦à§‡à§Ÿà¦¾', ''),
(25, 'Oily', 'à¦¤à§ˆà¦²à¦¾à¦•à§à¦¤', 'Greasy, fatty', '', 'feeling,tasting,smelling or looking like oil'),
(26, 'Norm', 'à¦†à¦¦à¦°à§à¦¶', 'Ideology,model', '', ''),
(27, 'Martial', 'à¦¸à¦¾à¦®à¦°à¦¿à¦•', 'Warlike, military', '', ''),
(28, 'Jam', 'à¦–à¦¾à¦¬à¦¾à¦° à¦‰à¦ªà¦¾à¦¦à¦¾à¦¨', 'Stuff', '', 'a kind of food item'),
(29, 'Introduce', 'à¦ªà¦°à¦¿à¦šà¦¿à¦¤ à¦•à¦°à¦¾à¦¨à§‹', 'Make known, acquaint', '', ''),
(30, 'Huge', 'à¦¬à¦¿à¦¶à¦¾à¦²', 'Large, great, bulky, gargantuan, immense, vast, prodigious, enormous, monstrous, colossal,', 'à¦¬à¦¿à¦ªà§à¦²', ''),
(31, 'Glory', 'à¦¯à¦¶', 'Exaltation, brilliance, pride, splendour', 'à¦®à¦¹à¦¿à¦®à¦¾', ''),
(32, 'Foundation', 'à¦­à¦¿à¦¤à§à¦¤à¦¿', 'base, basis, ground', '', ''),
(33, 'Expose', 'à¦¬à¦¿à¦¬à¦°à¦£', 'exhibit', 'à¦ªà§à¦°à¦¦à¦°à§à¦¶à¦¨,à¦‰à¦˜à¦¾à¦Ÿà¦¨', ''),
(34, 'Durable', 'à¦Ÿà§‡à¦•à¦¸à¦‡', '', '', ''),
(35, 'Control', 'à¦¨à¦¿à§Ÿà¦¨à§à¦¤à§à¦°à¦£ à¦•à¦°à¦¾', 'Regulate', '', ''),
(36, 'Business', 'à¦¬à§à¦¯à¦¬à¦¸à¦¾', '', '', ''),
(37, 'ALLAH', 'à¦†à¦²à§à¦²à¦¾à¦¹à§â€Œ', '', '', ''),
(38, 'else', 'à¦¨à¦šà§‡à§Ž', '', 'à¦¯à¦¦à¦¿ à¦à¦•à¦Ÿà¦¿ à¦à¦Ÿà¦¾ à¦¨à¦¾ à¦¹à§Ÿ à¦¤à¦¾à¦¹à¦²à§‡ à¦…à¦¨à§à¦¯ à¦•à¦¿à¦›à§ à¦à¦•à¦Ÿà¦¾ à¦¹à¦“à§Ÿà¦¾ à¦¬à¦¾ à¦•à¦°à¦¾ à¦¬à§à¦à¦¾à§Ÿ', ''),
(39, 'dictator', 'à¦¸à¦°à§à¦¬à¦¾à¦§à¦¿à¦¨à¦¾à§Ÿà¦•', 'martinet, absolute ruler, absolutist', '', ''),
(40, 'diction', 'à¦¶à¦¬à§à¦¦ à¦¨à¦¿à¦°à§à¦¬à¦¾à¦šà¦¨', '', 'à¦°à¦šà¦¨à¦¾ à¦°à§€à¦¤à¦¿', ''),
(41, 'die', 'à¦®à¦°à¦¾', '', '', ''),
(42, 'extol', 'à¦‰à¦šà§à¦š à¦ªà§à¦°à¦¶à¦‚à¦¸à¦¾ à¦•à¦°à¦¾', '', '', ''),
(43, 'oath', 'à¦¶à¦ªà¦¥', '', '', ''),
(44, 'plenty', 'à¦ªà§à¦°à¦¾à¦šà§à¦°à§à¦¯', '', '', ''),
(45, 'abase', 'à¦…à¦ªà¦®à¦¾à¦¨à¦¿à¦¤ à¦¹à¦“à§Ÿà¦¾', '', '', ''),
(46, 'elegant', 'à¦®à¦¾à¦°à§à¦œà¦¿à¦¤', '', 'à¦­à¦¦à§à¦°à¦¤à¦¾ à¦¬à¦œà¦¾à§Ÿ à¦°à§‡à¦–à§‡', ''),
(47, 'transcience', 'à¦•à§à¦·à¦£à¦¸à§à¦¥à¦¾à§Ÿà§€à¦¤à§à¦¤à¦¤à¦¾', '', '', ''),
(48, 'agony', 'à¦¦à§à¦ƒà¦¸à¦¹ à¦¯à¦¨à§à¦¤à§à¦°à¦£à¦¾', '', '', ''),
(49, 'foster', 'à¦²à¦¾à¦²à¦¨ à¦ªà¦¾à¦²à¦¨ à¦•à¦°à¦¾', '', '', ''),
(50, 'fondle', 'à¦†à¦¦à¦° à¦•à¦°à¦¾', '', '', ''),
(51, 'minimal', 'à¦ªà¦°à¦¿à¦®à¦¾à¦£à§‡ à¦¬à¦¾ à¦®à¦¾à¦¤à§à¦°à¦¾à§Ÿ à¦–à§à¦¬à¦‡ à¦•à¦®', '', '', 'adjective, very small in size or amount; as small as possible'),
(52, 'Abduct', 'à¦…à¦ªà¦¹à¦°à¦¨', '', '', 'adjective, very small in size or amount; as small as possible'),
(53, 'hangout', 'à¦¯à§‡à¦–à¦¾à¦¨à§‡ à¦•à§‡à¦‰ à¦ªà§à¦°à¦¾à§Ÿ à¦¯à§‡à¦¤à§‡ à¦ªà¦›à¦¨à§à¦¦ à¦•à¦°à§‡', 'a place where someone likes to go to often', '', ''),
(54, 'crack', 'à¦«à¦¾à¦Ÿà¦² à¦§à¦°à¦¾, à¦šà¦¿à§œ à¦–à¦¾à¦“à§Ÿà¦¾', 'to break or make something break', '', ''),
(55, 'discernible', 'à¦¨à¦¿à¦°à§à¦£à§Ÿ à¦¸à¦¾à¦§à§à¦¯', 'noticable, understandable', '', ''),
(56, 'sinister', 'à¦…à¦¶à§à¦­', '', 'à¦…à¦®à¦™à§à¦—à¦²à¦¸à§‚à¦šà¦•', 'making one you feel that something evil, dangerous or illegal'),
(57, 'actuate', 'à¦ªà§à¦°à¦£à§‹à¦¦à¦¿à¦¤ à¦•à¦°à¦¾', '', 'à¦ªà§à¦°à¦¬à¦¿à¦¤à§à¦¤ à¦•à¦°à¦¾', 'to behave in a particular way because of something'),
(58, 'retute', 'à¦­à§à¦² à¦ªà§à¦°à¦®à¦¾à¦¨ à¦•à¦°à¦¾', 'confute', '', ''),
(59, 'somewhat', 'à¦…à¦²à§à¦ª à¦¬à¦¾ à¦…à¦²à§à¦ªà§‡à¦° à¦šà§‡à§Ÿà§‡ à¦à¦•à¦Ÿà§ à¦¬à§‡à¦¶à¦¿', '', '', ''),
(60, 'eloquent', 'à¦¬à¦¾à¦•à¦ªà¦Ÿà§', '', '', ''),
(61, 'stable', 'à¦¸à§à¦¥à¦¾à§Ÿà§€', 'steady, regular, consistent, even, constant,', '', ''),
(62, 'coeval', 'à¦¸à¦®à¦•à¦¾à¦²', 'having the same age or date of origin', '', ''),
(63, 'provoke', 'à¦°à¦¾à¦—à¦¾à¦¨à§‹', '', 'à¦–à§‡à¦ªà¦¾à¦¨à§‹, à¦¬à¦¾à¦§à§à¦¯ à¦•à¦°à¦¾', ''),
(64, 'allure', 'à¦ªà§à¦°à¦²à§‹à¦­à¦¿à¦¤ à¦•à¦°à¦¾', 'attract, persuade, charm, win over, tempt, lure,seduce, entice,cajole', '', ''),
(65, 'unravel', 'à¦¸à¦®à¦¾à¦§à¦¾à¦¨ à¦•à¦°à¦¾', 'solve, explain, work out, figure out, make out', '', ''),
(66, 'confront', 'à¦¸à¦®à§à¦®à§à¦–à§€à¦¨ à¦¹à¦“à§Ÿà¦¾', 'face up to , meet head-on', '', ''),
(67, 'emission', 'à¦¨à¦¿à¦°à§à¦—à¦®à¦¨', 'outflow,diffusion, utterance', '', ''),
(68, 'collide', 'à¦§à¦¾à¦•à§à¦•à¦¾ à¦–à¦¾à¦“à§Ÿà¦¾', 'crash, clash, come into collision', '', ''),
(69, 'kaolin', 'à¦šà§€à¦¨à¦¾à¦®à¦¾à¦Ÿà¦¿', '', '', ''),
(70, 'sultry', 'à¦…à¦¸à§à¦¬à¦¸à§à¦¤à¦¿à¦¦à¦¾à§Ÿà¦• à¦—à¦°à¦®', '', '', ''),
(71, 'singleton', 'à¦à¦• à¦¬à¦¾ à¦à¦• à¦ªà§à¦°à¦•à¦¾à¦° à¦†à¦‡à¦Ÿà§‡à¦®', '', '', 'a single item that you are talking about(noun)'),
(72, 'bootstrap', 'à¦¨à¦¿à¦œà§‡à¦° à¦šà§‡à¦·à§à¦Ÿà¦¾à§Ÿ à¦‰à¦¨à§à¦¨à¦¤à¦¿ à¦•à¦°à¦¾', '', '', 'to improve your position and get out of a difficult situation by own efforts, without helps of others'),
(73, 'vulnerable', 'à¦†à¦•à§à¦°à¦®à¦¨ à¦¬à¦¾ à¦†à¦˜à¦¾à¦¤à§‡à¦° à¦¯à§‹à¦—à§à¦¯', 'someone who is vulnerable can easily be harmed or hurt', '', ''),
(74, 'needle in a haystack in php', 'à¦¬à§œ à¦•à§‹à¦¨ à¦¸à§à¦Ÿà§à¦°à¦¿à¦‚ à¦ à¦¸à¦¾à¦¬à¦¸à§à¦¤à§à¦°à¦¿à¦‚ à¦–à§‹à¦œà¦¾', '', '', 'Finding a needle in a haystack. You are looking for a substring in a bigger string.'),
(75, 'offset', 'à¦¶à¦¾à¦–à¦¾,à¦ªà§à¦°à¦¶à¦¾à¦–à¦¾', '', '', ''),
(76, 'ulcer', 'à¦•à§à¦·à¦¤', '', 'à¦˜à¦¾', ''),
(77, 'notation', 'à¦ªà§à¦°à¦¤à¦¿à¦¨à¦¿à¦§à¦¿à¦¤à§à¦¬ à¦•à¦¾à¦°à§€ à¦ªà§à¦°à¦¤à§€à¦•', '', '', 'a system of signs or symbols to represent information ex: scientific notation'),
(78, 'aside', 'à¦à¦• à¦ªà¦¾à¦¶à§‡', 'to one side, out of the way', '', ''),
(79, 'leverage', 'à¦ªà§à¦°à§‡à¦·à¦£à¦¾ à¦¦à§‡à§Ÿà¦¾', 'the power to influence', 'à¦ªà§à¦°à¦­à¦¾à¦¬à¦¿à¦¤ à¦•à¦°à¦¾à¦° à¦¸à¦¾à¦®à¦°à§à¦¥à§à¦¯', ''),
(80, 'arbitrary', 'à¦¸à§à¦¬à§‡à¦šà§à¦›à¦¾à¦šà¦¾à¦°à§€', '', 'à¦¬à¦¿à¦à¦§à¦¿ à¦¨à¦¿à¦°à¦ªà§‡à¦•à§à¦·', 'not seeming to be based on a reason,system or plan and sometimes seeming to be unfair'),
(81, 'dashboard', 'à¦¬à§‹à¦°à§à¦¡ à¦¯à§‡à¦–à¦¾à¦¨à§‡ à¦¬à¦¿à¦­à¦¿à¦¨à§à¦¨ à¦§à¦°à¦¨à§‡à¦° à¦Ÿà§à¦²à¦¸ à¦¥à¦¾à¦•à§‡', '', '', 'front place including instuments and controls in it'),
(82, 'nomenclature', 'à¦†à¦–à§à¦¯à¦¾', '', 'à¦ªà¦°à¦¿à¦­à¦¾à¦·à¦¾ , à¦…à¦­à¦¿à¦§à¦¾à¦¨', 'a system of naming things'),
(83, 'synchronous', 'à¦à¦•à¦‡ à¦¸à¦®à§Ÿà§‡ à¦˜à¦Ÿà¦¾', '', '', 'occurring or existing at the same time'),
(84, 'asynchronous', 'à¦à¦•à¦‡ à¦¸à¦®à§Ÿà§‡ à¦¨à¦¾ à¦˜à¦Ÿà¦¾', '', '', 'not existing or occurring at the same time'),
(85, 'slug', 'à¦§à¦¿à¦°à§‡ à¦šà¦²à¦¾ à¦ªà§à¦°à¦¾à¦£à§€', 'a small soft creature that moves very slowly', 'slug à¦¥à§‡à¦•à§‡  sluggish', 'à¦¯à¦¾à¦°à¦¾ à¦–à§à¦¬ à¦§à§€à¦°à¦—à¦¤à¦¿à¦° à¦²à§‹à¦• à¦¯à§‡ à¦•à§‹à¦¨ à¦•à¦¾à¦œ à¦•à¦°à§à¦®à§‡ , à¦à¦• à¦§à¦°à¦¨à§‡à¦° à¦ªà§à¦°à¦¾à¦£à§€ à¦†à¦›à§‡ à¦¯à¦¾ à¦–à§à¦¬ à¦§à§€à¦° à¦—à¦¤à¦¿à¦¤à§‡ à¦šà¦²à§‡ '),
(86, 'checkout', 'à¦¯à§‡à¦–à¦¾à¦¨à§‡ à¦ªà¦£à§à¦¯ à¦•à§‡à¦¨à¦¾à¦° à¦œà¦¨à§à¦¯ à¦Ÿà¦¾à¦•à¦¾ à¦¦à§‡à§Ÿà¦¾ à¦¹à§Ÿ', '', '', 'the place in a super market where you pay for the goods you have collected'),
(87, 'checklist', 'à¦†à¦‡à¦Ÿà§‡à¦®à§‡à¦° à¦¤à¦¾à¦²à¦¿à¦•à¦¾', '', '', 'a list that helps you by reminding you of the things you need to do or get for a particular job or activity'),
(88, 'enact', 'à¦…à¦­à¦¿à¦¨à§Ÿ à¦•à¦°à¦¾,', '', 'à¦†à¦‡à¦¨à§‡ à¦ªà¦°à¦¿à¦¨à¦¤ à¦•à¦°à¦¾', ''),
(89, 'troll', 'à¦˜à§à¦°à¦ªà¦¾à¦• à¦–à¦¾à¦“à§Ÿà¦¾', '', 'à¦œà§‹à¦° à¦—à¦²à¦¾à§Ÿ à¦—à¦¾à¦¨ à¦—à¦¾à¦“à§Ÿà¦¾', ''),
(90, 'aberration', 'à¦¨à§à¦¯à¦¾à§Ÿà¦ªà¦¥ à¦¹à¦‡à¦¤à§‡ à¦¬à¦¿à¦šà§à§Ÿà¦¤à¦¿', '', '', ''),
(91, 'sniff', 'à¦—à¦¨à§à¦§ à¦¨à§‡à¦“à§Ÿà¦¾ ', 'smell,scent', '', 'to smell something, get/catch a whiff of something'),
(92, 'comet', 'à¦§à§à¦®à¦•à§‡à¦¤à§', '', '', ''),
(93, 'counterfeit', 'à¦®à§‡à¦•à¦¿', '', 'à¦­à¦£à§à¦¡à¦¾à¦®à¦¿, à¦šà¦¾à¦²à¦¾à¦•à¦¿', ''),
(94, 'exempt', 'à¦…à¦¨à§à¦¶à§€à¦²à¦¨', '', 'à¦…à¦­à§à¦¯à¦¾à¦¸', ''),
(95, 'exemption', 'à¦®à§à¦•à§à¦¤à¦¿,', '', 'à¦°à§‡à¦¹à¦¾à¦‡', ''),
(96, 'exert', 'à¦ªà§à¦°à§Ÿà§‹à¦— à¦•à¦°à¦¾', '', 'à¦ªà§à¦°à¦šà§‡à¦·à§à¦Ÿà¦¾ à¦•à¦°à¦¾', ''),
(97, 'cake', 'à¦ªà¦¿à¦ à¦¾', '', '', ''),
(98, 'Development', 'à¦‰à¦¨à§à¦¨à§Ÿà¦¨', '', '', ''),
(99, 'denial', 'à¦…à¦§à¦¿à¦¬à¦¾à¦¸à§€', 'inhabitant', 'à¦¬à¦¾à¦¸à¦¿à¦¨à§à¦¦à¦¾', ''),
(100, 'ingress', 'à¦­à¦¿à¦¤à¦°à§‡ à¦ªà§à¦°à¦¬à§‡à¦¶,à¦ªà§à¦°à¦¬à§‡à¦¶à§‡à¦° à¦…à¦§à¦¿à¦•à¦¾à¦°', '', '', ''),
(101, 'inane', 'à¦…à¦ªà¦¦à¦¾à¦°à§à¦¥,à¦«à¦¾à¦à¦•à¦¾', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `mcq`
--

CREATE TABLE `mcq` (
  `id` int(10) NOT NULL,
  `question` varchar(250) DEFAULT NULL,
  `ans1` varchar(250) DEFAULT NULL,
  `ans2` varchar(250) DEFAULT NULL,
  `ans3` varchar(250) DEFAULT NULL,
  `ans4` varchar(250) DEFAULT NULL,
  `session_id` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bangla_english`
--
ALTER TABLE `bangla_english`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mcq`
--
ALTER TABLE `mcq`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bangla_english`
--
ALTER TABLE `bangla_english`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;
--
-- AUTO_INCREMENT for table `mcq`
--
ALTER TABLE `mcq`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
DELIMITER $$
--
-- Events
--
CREATE DEFINER=`root`@`localhost` EVENT `delete_mcq` ON SCHEDULE EVERY 2 DAY STARTS '2017-04-21 23:15:36' ON COMPLETION NOT PRESERVE ENABLE DO delete from mcq where date<curdate()$$

DELIMITER ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
